import { batchLog } from "../batch";
import { logConfig } from "../config";

interface LogData {
  [k: string]: unknown;
}

export interface LogOptions {
  logSessionId: string;
}

export interface UserInfo {
  id: string;
}

export interface UserAgent extends Record<string, unknown> {
  platform: string;
  app_version: string;
  user_info?: UserInfo;
}

interface Payload {
  module: string;
  text?: string;
  event?: string;
  data?: any;
}

export type EnvType = "dev" | "staging" | "prod";

export type LogType =
  | "debug"
  | "info"
  | "warning"
  | "error"
  | "critical"
  | "track";

export interface LogStructure
  extends Omit<LogOptions, "getClientFingerprint" | "getUserAgent"> {
  timestamp: number;
  type: LogType;
  payload: Payload;
}

type PrintLogHandlerType = (logStructure: LogStructure) => void;

export class Log {
  private namespace: string;
  private options: LogOptions;
  private printLogHandler?: PrintLogHandlerType;

  constructor(namespace: string, options: LogOptions) {
    this.namespace = namespace;
    this.options = options;
  }

  private getLogStructure(
    type: LogType,
    text: string,
    data?: LogData
  ): LogStructure {
    const structure: LogStructure = {
      timestamp: new Date().getTime(),
      logSessionId: this.options.logSessionId,
      type,
      payload: {
        module: this.namespace,
        text: type !== "track" ? text : undefined,
        event: type === "track" ? text : undefined,
        data,
      },
    };

    return structure;
  }

  private printLog(logStructure: LogStructure): void {
    // never print log on prod
    if (logConfig.env === "prod") {
      return;
    }

    if (typeof this.printLogHandler === "function") {
      this.printLogHandler(logStructure);
      return;
    }

    // default handler
    let style = "";

    switch (logStructure.type) {
      case "info":
        style = "color: #bada55; background: black;";
        break;
      case "debug":
        style = "color: #1174DC; background: black;";
        break;
      case "warning":
        style = "color: yellow; background: black;";
        break;
      case "track":
        style = "color: #EA8F25; background: black;";
        break;
      case "error":
        style = "color: white; background: red;";
        break;
      case "critical":
        style = "color: white; background: red; font-weight: bold;";
        break;
      default:
        style = "color: white; background: black;";
    }

    const time = new Date(logStructure.timestamp);
    const timeStr = `${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}:${time.getMilliseconds()}`;

    console.log(
      `${timeStr} Log-SDK %c[${logStructure.type.toUpperCase()}] - ${
        logStructure.payload.module
      }`,
      style,
      logStructure.payload.text || logStructure.payload.event,
      logStructure.payload.data !== undefined ? logStructure.payload.data : ""
    );
  }

  private base(type: LogType, text: string, data?: LogData): void {
    if (typeof text !== "string") {
      console.error(
        "First param of log must be a string (free text or event name)",
        text
      );
    }

    if (type !== "debug" && data && !(data instanceof Object)) {
      console.error(
        "Second param of log must be an object (this param is optional)",
        data
      );
    }

    const logStructure = this.getLogStructure(type, text, data);

    this.printLog(logStructure);

    if (type !== "debug") {
      batchLog.addToBatch(logStructure);
    }
  }

  /**
   * Override default print log function
   * @param handler (logStructure: ILogStructure) => void
   */
  public overridePrintLog(handler: PrintLogHandlerType): void {
    this.printLogHandler = handler;
  }

  public debug(text: string, data?: any): void {
    if (arguments.length > 2) {
      console.error(
        "Log only support 2 params: first is text & second is data object"
      );
    }
    this.base("debug", text, data);
  }

  public info(text: string, data?: LogData): void {
    if (arguments.length > 2) {
      console.error(
        "Log only support 2 params: first is text & second is data object"
      );
    }
    this.base("info", text, data);
  }

  public warning(text: string, data?: LogData): void {
    if (arguments.length > 2) {
      console.error(
        "Log only support 2 params: first is text & second is data object"
      );
    }
    this.base("warning", text, data);
  }

  public error(text: string, data?: LogData): void {
    if (arguments.length > 2) {
      console.error(
        "Log only support 2 params: first is text & second is data object"
      );
    }
    this.base("error", text, data);
  }

  public critical(text: string, data?: LogData): void {
    if (arguments.length > 2) {
      console.error(
        "Log only support 2 params: first is text & second is data object"
      );
    }
    this.base("critical", text, data);
  }

  public track(text: string, data?: LogData): void {
    if (arguments.length > 2) {
      console.error(
        "Log only support 2 params: first is text & second is data object"
      );
    }
    this.base("track", text, data);
  }
}
