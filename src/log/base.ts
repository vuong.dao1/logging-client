import { logConfig } from "../config";
import { randomId } from "../randomId";
import { printDebug } from "../utils";
import { Log } from "./log";
import type { EnvType, UserAgent } from "./log";
import type { LogConfigOptions } from "../config";
import { setBaseUrl } from "../http";

type LogConfig = Omit<
  LogConfigOptions,
  "env" | "service" | "fingerprint" | "userAgent"
>;
export class BaseLog {
  private logSessionId: string;

  constructor(enviroment: EnvType, service: string, config?: LogConfig) {
    this.logSessionId = this.createSessionId();

    this.applyConfig(enviroment, service, config);
    printDebug(`env: ${enviroment}, service: ${service}`);
  }

  private applyConfig(
    enviroment: EnvType,
    service: string,
    config?: LogConfig
  ) {
    logConfig.setEnv(enviroment);
    logConfig.setService(service);

    if (config?.batchIntervalMs) {
      logConfig.setBatchIntervalMs(config.batchIntervalMs);
      printDebug("batchIntervalMs", logConfig.batchIntervalMs);
    }

    if (config?.batchLimit) {
      logConfig.setBatchLimit(config.batchLimit);
      printDebug("batchLimit", logConfig.batchLimit);
    }

    if (config?.logServerUrl) {
      logConfig.setLogServerUrl(config.logServerUrl);
      setBaseUrl(config.logServerUrl);
      printDebug("logServerUrl", logConfig.logServerUrl);
    }
  }

  private createSessionId(): string {
    const logSessionId = randomId();

    printDebug("create session id", logSessionId);
    return logSessionId;
  }

  public setClientFingerprint(clientFingerprint: string): void {
    logConfig.setFingerprint(clientFingerprint);
    printDebug("set client fingerprint", logConfig.fingerprint);
  }

  public setUserAgent(userAgent: UserAgent): void {
    logConfig.setUserAgent(userAgent);
    printDebug("set user agent", logConfig.userAgent);
  }

  public createLog(namespace: string): Log {
    const log = new Log(namespace, {
      logSessionId: this.logSessionId,
    });

    return log;
  }
}
