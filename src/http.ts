import axios, { AxiosRequestConfig } from "axios";
import { logConfig } from "./config";
import { printDebug } from "./utils";

export const http = axios.create({
  baseURL: logConfig.logServerUrl,
  headers: {
    "Content-Type": "application/json",
  },
});

http.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    printDebug(`${config.method} ${config.baseURL} ${config.url}`, config.data);
    return config;
  },
  (error) => Promise.reject(error)
);

export const setBaseUrl = (baseUrl: string): void => {
  http.defaults.baseURL = baseUrl;
};
