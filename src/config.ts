import type { EnvType, UserAgent } from "./log/log";

export interface LogConfigOptions {
  env: EnvType;
  service: string;
  logServerUrl?: string;
  userAgent?: UserAgent;
  fingerprint?: string;
  batchLimit?: number;
  batchIntervalMs?: number;
}

export class LogConfig {
  private config: LogConfigOptions;

  constructor() {
    // default config
    this.config = {
      env: "dev",
      service: "",
      logServerUrl: "https://jam-log.autonomous.ai/v1",
      batchLimit: 10,
      batchIntervalMs: 5000,
    };
  }

  get env() {
    return this.config.env;
  }

  get userAgent() {
    return this.config.userAgent;
  }

  get fingerprint() {
    return this.config.fingerprint;
  }

  get logServerUrl() {
    return this.config.logServerUrl;
  }

  get batchIntervalMs() {
    return this.config.batchIntervalMs;
  }

  get batchLimit() {
    return this.config.batchLimit;
  }

  get service() {
    return this.config.service;
  }

  setEnv(env: EnvType) {
    this.config.env = env;
  }

  setUserAgent(userAgent: UserAgent) {
    this.config.userAgent = userAgent;
  }

  setFingerprint(fingerprint: string) {
    this.config.fingerprint = fingerprint;
  }

  setService(service: string) {
    this.config.service = service;
  }

  setBatchLimit(batchLimit: number) {
    this.config.batchLimit = batchLimit;
  }

  setBatchIntervalMs(intervalMs: number) {
    this.config.batchIntervalMs = intervalMs;
  }

  setLogServerUrl(logServerUrl: string) {
    this.config.logServerUrl = logServerUrl;
  }
}

export const logConfig = new LogConfig();
