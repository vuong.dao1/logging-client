import { http } from "../src/http";
import { logConfig } from "./config";
import { printDebug } from "./utils";
import type { LogStructure } from "./log/log";

export class BatchLog {
  private batch: LogStructure[];

  constructor() {
    this.batch = [];

    this.scheduleSend();
  }

  private async sendBatchLog(batchLogs: Array<LogStructure>): Promise<void> {
    const listLogs = batchLogs.map((logStructure) => {
      const x = {
        payload: logStructure.payload,
        severity: logStructure.type,
        client_id: logConfig.fingerprint || logStructure.logSessionId,
        env: logConfig.env,
        timestamp_utc: logStructure.timestamp,
        service: logConfig.service,
        user_agent: logConfig.userAgent
          ? {
              ...logConfig.userAgent,
              log_session_id: logStructure.logSessionId,
            }
          : undefined,
      };
      return x;
    });

    await http.post("", {
      list_log: listLogs,
    });
    printDebug("sent log", listLogs);
  }

  private async scheduleSend(): Promise<void> {
    setTimeout(async () => {
      printDebug("Curent batch length", this.batch.length);
      // get a list of logs to send (number from CONFIG)
      const subBatch = this.batch.splice(0, logConfig.batchLimit);

      try {
        if (subBatch.length) {
          printDebug("Sub batch length", subBatch.length);
          await this.sendBatchLog(subBatch);
        } else {
          // abort
        }
      } catch (e) {
        // send failed, restore sub batch
        this.batch = [...subBatch, ...this.batch];
        printDebug("send log failed, restored batch", this.batch.length);
      } finally {
        this.scheduleSend();
      }
    }, logConfig.batchIntervalMs);
  }

  addToBatch(log: LogStructure): void {
    this.batch.push(log);
  }
}

export const batchLog = new BatchLog();
