export const randomId = (): string => {
  return (
    "client-id-" +
    Math.random().toString(16).substr(2, 9) +
    new Date().getTime()
  );
};
