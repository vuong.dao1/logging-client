import { BaseLog } from "./log/base";
import { version } from "../package.json";

console.log("LOG version = ", version);

export { BaseLog as LogService };
