import { logConfig } from "./config";

export const printDebug = (...args: unknown[]): void => {
  if (logConfig.env === "dev") {
    console.debug("LOG", ...args);
  }
};
