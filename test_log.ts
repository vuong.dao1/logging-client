/* eslint-disable @typescript-eslint/camelcase */
import { LogService } from "./lib/src/index";

const logService = new LogService("dev", "music-service", { logServerUrl: 'http://aaa.com', batchLimit: 3, batchIntervalMs: 2000 });

const logA = logService.createLog("TEST");
const logB = logService.createLog("TEST1");

logService.setUserAgent({
  platform: "desktop",
  app_version: "1.0",
  user_info: {
    id: "user-1"
  }
});

logService.setClientFingerprint("f-123");

for (let i = 0; i < 10; i++) {
  logA.info("test-" + i);
}

setTimeout(() => {
  for (let i = 0; i < 10; i++) {
    logB.info("new test---" + i);
  }
}, 15000);
