declare module "json-truncate" {
  function jsonTruncate(obj: any, options: any): string;

  export = jsonTruncate;
}
