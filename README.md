Logging Client for Autonomous (support Web, NodeJs, React Native, ElectronJS)

```javascript
import { LogService } from 'logging-client';

// config
const env = 'dev'; // "dev" | "staging" | "prod"
const serviceName = 'service_name';
const options = { logServerUrl: 'http:...', batchLimit: 3, batchIntervalMs: 2000 };
const Log = new LogService(env, serviceName, options);

// set user agent (optional)
Log.setUserAgent({
  platform: "desktop",
  app_version: "1.0",
  user_info: {
    id: "user-1"
  }
});

// set client fingerprint (optional)
Log.setClientFingerprint("f-123");


// create log with namespace
const moduleALog = Log.createLog('module-A');

moduleALog.debug('debug');
moduleALog.info('info');
moduleALog.warning('warning');
moduleALog.error('error');
moduleALog.critical('critical');
moduleALog.track('track');
```
