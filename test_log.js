"use strict";
exports.__esModule = true;
/* eslint-disable @typescript-eslint/camelcase */
var index_1 = require("./lib/src/index");
var logService = new index_1.LogService("dev", "music-service", { logServerUrl: 'http://aaa.com', batchLimit: 3, batchIntervalMs: 2000 });
var logA = logService.createLog("TEST");
var logB = logService.createLog("TEST1");
logService.setUserAgent({
    platform: "desktop",
    app_version: "1.0",
    user_info: {
        id: "user-1"
    }
});
logService.setClientFingerprint("f-123");
for (var i = 0; i < 10; i++) {
    logA.info("test-" + i);
}
setTimeout(function () {
    for (var i = 0; i < 10; i++) {
        logB.info("new test---" + i);
    }
}, 15000);
